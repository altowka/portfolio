
/////////////////fixed-nav

    function fixedNav(){
    
        var scrolltop = $(window).scrollTop();
        var hTop = 0;
        
        if(scrolltop > hTop && !$('.header-box').hasClass('scrolled') ){
            $('.header-box, .mobile-nav').addClass('scrolled');
        }
        if(scrolltop <= hTop && $('.header-box').hasClass('scrolled') ){
            $('.header-box, .mobile-nav').removeClass('scrolled');
        }
            
    }
    
    fixedNav();
    
    $(window).scroll(function(){
        requestAnimationFrame(function(){
            fixedNav();
        });
    });


    /////////////nav-navigation
    var navHeight = $(".header-box").height() + 44;
    

		$(".menu-item").click(function (e) {
            console.log(navHeight);
			e.preventDefault()
			let id_faq = $(this).attr('data-scroll-menu');
			let target = '.content-' + id_faq;

			$('html,body').animate({
				scrollTop: $(target).offset().top - navHeight
            }, 'slow');
            
     
        });
        

        ////////////hamburger

        
        $('body').on('click', '.hamburger', function (e) {
			$('.mobile-menu-absolute').show();
        });
        
        $('body').on('click', '.mobile-menu-absolute_close', function (e) {
			$('.mobile-menu-absolute').hide();
        });
        $('body').on('click', '.mobile-menu-absolute>ul>li', function (e) {
			$('.mobile-menu-absolute').hide();
        });
        
        // $(".hamburger").click(function (e) {
        //     e.preventDefault();
        //     $('.mobile-menu-absolute').show();
        // });
		// $('body').on('click', '.cross', function (e) {
		// 	$('.menu-inside').hide();
        // });
        


/////////////////////fade-in

(function($) {

    /**
     * Copyright 2012, Digital Fusion
     * Licensed under the MIT license.
     * http://teamdf.com/jquery-plugins/license/
     *
     * @author Sam Sehnert
     * @desc A small plugin that checks whether elements are within
     *     the user visible viewport of a web browser.
     *     only accounts for vertical position, not horizontal.
     */
  
    $.fn.visible = function(partial) {
      
        var $t            = $(this),
            $w            = $(window),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height() + 88,
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;
      
      return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
  
    };
      
  })(jQuery);



  $(window).scroll(function(event) {
  
    $(".module").each(function(i, el) {
      var el = $(el);
      if (el.visible(true)) {
        el.addClass("come-in"); 
      } 
    });
    
  });


  var win = $(window);
var allMods = $(".module");

// Already visible modules
allMods.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("come-in"); 
    } 
  });
  
});